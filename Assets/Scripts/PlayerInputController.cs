using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInputController : MonoBehaviour
{
    private Rigidbody2D rb;
    private PhotonView photonView;

    public float speed;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        photonView = GetComponent<PhotonView>();
    }

    private void FixedUpdate()
    {

        if (photonView.IsMine)
        {
            rb.AddForce(new Vector2(GameSceneManager.instance.PlayerJoyStick.Horizontal, GameSceneManager.instance.PlayerJoyStick.Vertical) * speed);
        }

    }
}
