using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class TestConnection : MonoBehaviourPunCallbacks
{

    public static TestConnection instance;
    public bool isHost;
 
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("starts....");
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.NickName = "Player" + Random.Range(1111, 9999);
    }



    #region override methods

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connecteed " + PhotonNetwork.NickName);
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        SceneManager.LoadScene("Lobby");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("couldn't connect because: " + cause.ToString());
    }

    public override void OnJoinedRoom()
    {
        // SceneManager.LoadScene("Game");
        Debug.Log("joining room");
       PhotonNetwork.LoadLevel("Game");
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("Join room failed! return code: " + returnCode + " \n message: " + message.ToString());
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Random join failed! return code: " + returnCode + " \n message: " + message.ToString());
        // create a new room if failed
        string roomName = PhotonNetwork.NickName + Random.Range(1000, 9999);
        Debug.Log("creating room " + roomName);

        PhotonNetwork.CreateRoom(roomName);
    }

    public override void OnCreatedRoom()
    {
        isHost = true;
    }

    #endregion




    public void CreateRoom(string RoomName)
    {
        PhotonNetwork.CreateRoom(RoomName);
    }

    public void JoinRoom(string RoomName)
    {
        PhotonNetwork.JoinRoom(RoomName);
        
    }

    public void JoinRandomRoom() {
        PhotonNetwork.JoinRandomRoom();
    }


  
}
