using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;


public class GameSceneManager : MonoBehaviour
{
    public static GameSceneManager instance;

    public Text PlayerName;
    public VariableJoystick PlayerJoyStick;

    public Transform homePosition;
    public Transform awayPosition;


    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        
        SpawnPlayer();
    }

    private void SpawnPlayer()
    {
        //Vector2 pos = new Vector2(Random.Range(-10, 10), Random.Range(-10, 10));
        //PhotonNetwork.Instantiate("Player", pos, Quaternion.identity);

        if (TestConnection.instance.isHost)
        {
            PhotonNetwork.Instantiate("Player", homePosition.position, Quaternion.identity);
        }
        else
        {
            PhotonNetwork.Instantiate("Player", awayPosition.position, Quaternion.identity);
        }
        PlayerName.text = PhotonNetwork.NickName;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
