using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbySceneManager : MonoBehaviour
{
    public InputField CreateRoomName;
    public InputField JoinRoomName;


    public void OnClickCreateRoom()
    {
        TestConnection.instance.CreateRoom(CreateRoomName.text);
    }

    public void OnClickJoinRoom()
    {
        TestConnection.instance.JoinRoom(JoinRoomName.text);
    }

    public void OnClickJoinRandomRoom()
    {
        TestConnection.instance.JoinRandomRoom();
    }

    
}
